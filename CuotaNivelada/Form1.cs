﻿using CuotaNivelada.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuotaNivelada
{
    public partial class FrmPrincipal : Form
    {
        int j;

        public FrmPrincipal()
        {
            InitializeComponent();
        }
         
        private void AgregarData(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Validaciones
            if (txtPrestamo == null)
            {
                MessageBox.Show("Debe Digitar todos los datos");
                return;
            }

            if (txtPeriodo.Text == null)
            {
                MessageBox.Show("Debe Digitar todos los datos");
                return;
            }

            if (txtTasa.Text == null)
            {
                MessageBox.Show("Debe digitar todos los datos");
                return;
            }
            
            // Declaracion de Variables
            int N = Convert.ToInt32(txtPeriodo.Text); //Periodos
            double MontoDeuda = double.Parse(txtPrestamo.Text); //MONTO DEUDA
            double Tasa = double.Parse(txtTasa.Text);//MONTO DEL LA TASA
            double Cuota;
            double Interes = 0;
            double Amortizacion = 0;
            double ValorActual = MontoDeuda;
            //Adicionamos un nuevo renglon
           
            for (int i = 0; i <= N; i++)
            {
            
                j = dgCalendarioPagos.Rows.Add();
                dgCalendarioPagos.Rows[j].Cells[0].Value = i;

                if (i == 0) {
                    dgCalendarioPagos.Rows[j].Cells[4].Value = ValorActual;
                }
                if(i > 0)
                {
                    //==================================||Cuota||====================================================||
                    Cuota = Math.Round((Tasa * Math.Pow((1 + Tasa) , N) * MontoDeuda / (Math.Pow((1 + Tasa) , N) - 1)), 3);
                    dgCalendarioPagos.Rows[j].Cells[1].Value = Cuota;
                    //===============================================================================================||
                    //==================================||Interes||==================================================||
                    Interes = Math.Round(Tasa * ValorActual, 3);
                    dgCalendarioPagos.Rows[j].Cells[2].Value = Interes;
                    //===============================================================================================||
                    //==================================||Amottizacion||=============================================||
                    Amortizacion = Math.Round(Cuota - Interes, 3);
                    dgCalendarioPagos.Rows[j].Cells[3].Value = Amortizacion;
                    //===============================================================================================||
                    //==================================||Valor en Libro||===========================================||
                    ValorActual = Math.Round(ValorActual - Amortizacion, 3);
                    dgCalendarioPagos.Rows[j].Cells[4].Value = ValorActual;
                    //===============================================================================================||
                }
            }//final del For
        }

        private void GeneraReporte(object sender, EventArgs e)
        {

        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }
    }//final de la clase del Frame
}
