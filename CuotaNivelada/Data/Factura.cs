﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuotaNivelada.Factura
{
    class ReporteFactura
    {
        //Factura
        private double Prestamo;
        private double Plazo;
        private double tasa;
        //Detalle factura
        private int periodo;
        private double AbonoDeuda;
        private double interes;
        private double cuota;
        private double saldo;


        public ReporteFactura()
        {

        }
        
        public ReporteFactura(double MontoPrestamo, double PlazoPrestamo, double TasaPrestamo, int N, double AmortizacionDeuda, double Interes, double Cuota, double saldo)
        {
            this.Prestamo = MontoPrestamo;
            this.Plazo = PlazoPrestamo;
            this.tasa = TasaPrestamo;
            this.periodo = N;
            this.AbonoDeuda = AmortizacionDeuda;
            this.interes = Interes;
            this.cuota = Cuota;
            this.saldo = saldo;
        }


        //METODOS GETTER AND SETTER
        public double PrestamoDeuda
        {
            get
            {
                return Prestamo;
            }

            set
            {
                Prestamo = value;
            }
        }

        public double PlazoPrestamo
        {
            get
            {
                return Plazo;
            }
            set
            {
                Plazo = value;
            }
        }

        public double TasaPrestamo
        {
            get
            {
                return tasa;
            }
            set
            {
                tasa = value;
            }
        }

        public int PeriodoPrestamo
        {
            get
            {
                return periodo;
            }
            set
            {
                periodo = value;
            }
        }
        
        public double AbonoAlaDeuda
        {
            get
            {
                return AbonoDeuda;
            }
            set
            {
                AbonoDeuda = value;
            }
        }

        public double Interes
        {
            get
            {
                return interes;
            }
            set
            {
                interes = value;
            }
        }

        public double Cuota
        {
            get
            {
                return cuota;
            }
            set
            {
                cuota = value;
            }
        }

        public double Saldo
        {
            get
            {
                return saldo;
            }
            set
            {
                saldo = value;
            }
        }
    }//final de la clase
}
