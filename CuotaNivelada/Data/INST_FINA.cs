﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuotaNivelada.Data
{
    class INST_FINA
    {

        private double ValorEnLibro;
        private double TasaPrestatista;
        private double NumeroPeriodos;

        public INST_FINA() {

        }

        public INST_FINA(double Monto, double Tasa, double N)
        {
            this.ValorEnLibro = Monto;
            this.TasaPrestatista = Tasa;
            this.NumeroPeriodos = N;
        }

        public double ValorenLibro { get => ValorEnLibro; set => value = ValorEnLibro; }
        public double Tasa { get => TasaPrestatista; set => value = TasaPrestatista; }
        public double Periodo { get => NumeroPeriodos; set => value = NumeroPeriodos; }
    }
}
