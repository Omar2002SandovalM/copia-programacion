﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuotaNivelada.Data
{
    class Calculos
    {

        public static double Prestamo (double Prestamo)
        {
            return Prestamo;
        }

        public static double Tasa (double Tasa)
        {
            return Tasa;
        }

        public static double Periodos(double N)
        {
        
            return N;
        }
        //==============================================||Calculos||========================================||
        public static double CuotaDeduda(int n, double tasa, double prestamo)
        {
            double pago;
            
               
            double Dividendo = 1 - Math.Pow((1 + tasa), (-n));
            pago = prestamo * (tasa / Dividendo);
                
            
            return pago;
        }
        public static double InteresDeuda(double tasa, double prestamo)
        {

            return tasa * prestamo;
        }

        public static double Amortizacion(int n, double tasa, double prestamo)
        {
            return CuotaDeduda( n, tasa,  prestamo) - InteresDeuda(tasa, prestamo);
        }

        public static double ValorActual( double prestamo, int n, double tasa)
        {

            return prestamo - Amortizacion(n, tasa, prestamo); 
        }
    }//final de la clase
}//final de la solucion